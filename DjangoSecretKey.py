import sublime, sublime_plugin
import string
import random

class DjangoSecretKeyCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
        s = ''.join(random.choice(chars) for _ in range (50))
        cursor_pos = self.view.sel()[0].begin()
        self.view.insert(edit, cursor_pos, s)
